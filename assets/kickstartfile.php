<?php
/**
 * Sample Kickstartfile
 * 
 * @author Bernhard Baumrock, baumrock.com
 * @version 1
 * 
 * This file returns a php array with settings for the kickstart installer.
 * 
 * The installer instance is available as $this
 * You can access all methods of the installer, eg $this->randomPassword();
 */

$password = $this->randomPassword(16);

return [
  /**
   * url of the processwire installation zipfile
   * current master version: https://github.com/processwire/processwire/archive/master.zip
   * current dev version: https://github.com/processwire/processwire/archive/dev.zip
   * other versions: eg https://github.com/processwire/processwire/archive/3.0.34.zip
   */
  'pw' =>'https://github.com/processwire/processwire/archive/dev.zip',
    
  /**
   * which site profile to use
   * ---
   * options are:
   *  - a pw profile name, eg 'site-blank'
   *  - a local zip folder in the site root, eg 'myprofile.zip'
   *  - an url to a zip file, eg 'https://raw.githubusercontent.com/.../myprofile.zip'
   */
  'profile' => 'site-default',

  /**
   * installation settings
   * ---
   * here you can overwrite the default settings that are set in Kickstart.php
   * see the link below to toggle a list of all default settings
   */
  'settings' => [
    'timezone' => 368, // vienna
    
    //'dbName' => 'yourdbname',
    'dbUser' => 'root',
    'dbPass' => '', //$this->randomPassword(),
    
    //'admin_name' => 'admin',
    //'username' => 'admin',

    // random password set on top of this file
    //'userpass' => $password,
    //'userpass_confirm' => $password,

    //'dbTablesAction' => 'remove',
  ],
  
  /**
   * here you can set an array of options that is sent to the recipes
   * this makes it possible to define some kind of master recipes hosted on
   * gitlab and execute them with custom settings
   */
  'recipesettings' => [
    'foo' => 'bar',
  ],

  /**
   * recipes that are executed after installation
   * those recipes will get downloaded to the /recipes folder and then executed in order of their filename
   */
  'recipes' => [
    // downloads file to /recipes/sample-recipe.php and executes it
    // 'https://gitlab.com/baumrock/kickstart/raw/master/assets/sample-recipe.php',

    // sample callback as recipe
    function() {
      // $this->msg('Installing AOS...'); // demo
      $aos = $this->installModule('AdminOnSteroids', 'https://github.com/rolandtoth/AdminOnSteroids/archive/master.zip');
      $this->wire->modules->saveConfig($aos, [
        'enabled' => 1,
        'enabledSubmodules' => ['FieldAndTemplateEditLinks'],
      ]);
      
      $tracy = $this->installModule('TracyDebugger', 'https://github.com/adrianbj/TracyDebugger/archive/master.zip');
      $this->wire->modules->saveConfig($tracy, [
        'superuserForceDevelopment' => 1,
        'editor' => 'vscode://file/%file:%line',
      ]);
      
      // modules
      $this->installModule('SessionHandlerDB');
      $this->installModule('ProcessWireUpgrade', 'https://github.com/ryancramerdesign/ProcessWireUpgrade/archive/master.zip');
      
      // fields
      $this->installModule('FieldtypeRepeater');
      $this->installModule('InputfieldPageAutocomplete');
      
      // backups
      $this->installModule('ProcessDatabaseBackups','https://github.com/ryancramerdesign/ProcessDatabaseBackups/archive/master.zip');
      $this->installModule('LazyCron');
      $cron = $this->installModule('CronjobDatabaseBackup','https://github.com/kixe/CronjobDatabaseBackup/archive/master.zip');
      $this->wire->modules->saveConfig($cron, [
        'cycle' => 'everyHour',
        'max' => 50,
      ]);
    },
  ],
];
