<?php namespace ProcessWire;
/**
 * ProcessWire Installation Kickstarter
 * 
 * @author Bernhard Baumrock, baumrock.com
 * MIT Licence
 */
class Kickstart {

  private $wire; // processwire instance
  private $messages = []; // array of all alert messages
  private $numerr = 0; // number of errors
  private $screen; // which scree to show
  private $id; // unique id to identify this instance
  private $postStep = 0; // pw installation step counter
  private $host; // host of this site
  private $summary; // flag set at the end of the installer

  public $defaults; // default installer settings
  public $siteRoot; // root folder of this website or pw installation (with trailing slash)
  public $moduleUrl; // root folder of this processwire installation
  public $config; // kickstart installer config
  public $pwconfig; // merged processwire config (defaults + installer settings)
  public $version; // version number
  public $samplekickstartfile = 'https://gitlab.com/baumrock/kickstart/raw/master/assets/kickstartfile.php'; // url to sample kickstart file
  public $start; // start time to measure installation duration

  /**
   * class constructor
   */
  public function __construct($wire = null) {
    $this->start = microtime(true);

    ini_set('max_execution_time', 60*5);
    $this->id = uniqid();
    $this->version = '1.1';
    
    // load kickstartfile from url
    if(@$_POST['kickurl']) {
      $this->downloadAndSave($_POST['kickurl'], 'kickstartfile.php');
    }

    // update kickstartfile if "install" button was pressed
    if(@$_POST['install']) {
      if(!is_dir('bak')) mkdir('bak');
      file_put_contents('bak/'.date('ymdHis').'_kickstartfile.php', @file_get_contents('kickstartfile.php'));
      file_put_contents('kickstartfile.php', $_POST['kickstartfile']);
    }
    
    // set the root path
    if($wire) {
      // we are in a module context (while using the kickstart recipe editor)
      $this->siteRoot = $wire->config->paths->root;
      $this->moduleUrl = __DIR__;
    }
    else {
      $this->siteRoot = dirname($_SERVER['SCRIPT_FILENAME']).'/';
    }

    // load tracy debugger if present
    $this->tracy();

    // set the processwire instance
    $this->wire = $wire;
    if(!$wire AND is_file($this->siteRoot . 'index.php')) {
      // seems we are in the pw root directory so we can bootstrap it
      include($this->siteRoot . 'index.php');
      $this->wire = $wire;
    }

    /*********** only for kickstart installer (not the process module) */
      if($this->moduleUrl) return;

      // setup url + subdir
      $url = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
      $url = explode('/', $url);
      array_pop($url);
      $url = implode('/', $url);

      // set defaults
      $this->host = $url;
      $dbname = strtok(ltrim($this->host, 'www.'), '.');
      $hosts = "www.".ltrim($this->host, 'www.')."\n".ltrim($this->host, 'www.');
      $pass = $this->randomPassword(16);
      $this->defaults = [
        // site config
        'chmodDir' => 755,
        'chmodFile' => 644,
        'timezone' => null,
        'httpHosts' => $hosts,
        'dbName' => $dbname,
        'dbUser' => ini_get("mysqli.default_user") ?: $dbname,
        'dbPass' => ini_get("mysqli.default_pw") ?: '',
        'dbHost' => ini_get("mysqli.default_host") ?: 'localhost',
        'dbPort' => ini_get("mysqli.default_port") ?: 3306,
        'dbEngine' => 'MyISAM',
        'dbCharset' => 'utf8',
        'dbTablesAction' => null, // or ignore|remove

        // admin login
        'admin_name' => 'admin',
        'username' => 'admin',
        'userpass' => $pass,
        'userpass_confirm' => $pass,
        'useremail' => null,
        'remove_items' => [
          'install-php',
          'install-dir',
          'gitignore',
          'site-beginner',
          'site-languages',
          'site-blank',
          'site-classic',
          'site-default',
          'site-regular',
        ],
      ];

      $this->config = $this->getConfig();
      $this->pwconfig = @array_merge($this->defaults, $this->config['settings']);

      $this->install();
  }

  /**
   * execute the installation process
   */
  private function install() {
    // return if the install button was not pressed
    if(!@$_POST['install']) return;

    // return if we don't have a config array (eg syntax error)
    if(!$this->config OR !$this->pwconfig) return;

    // download pw from url
    $this->downloadAndSave($this->config['pw'], 'pw.zip');
    $files = $this->extractZip("pw.zip");
    $this->recursiveMove($files[0], $this->siteRoot); // move all files to the siteroot

    // set the active site profile
    $this->setSiteprofile();

    // do a quiet compatibility check to ensure that the .htaccess is in place
    // we define an empty errors array to never throw an error
    $this->postToPW(['step'=>1], ['stepname' => 'Check compatibility', 'errors' => [], 'quiet' => true]);

    // do all the pw installation steps
    if($this->postToPW(['step'=>1], ['stepname' => 'Check compatibility'])) {
      if($this->postToPW(['step'=>4], ['stepname' => 'Setup database'])) {
        if($this->postToPW(['step'=>5], ['stepname' => 'Create adminaccount'])) {
          if($this->executeRecipes()) {
            $this->cleanup();
            $this->summary = true;
    }}}}
  }
  
  /****************************** output *******************************/
  
    /**
     * render messages
     */
    public function renderMessages($card = false) {
      $out = '';
      foreach($this->messages as $message) {
        switch($message[0]) {
          case 'msg': $cls = ''; $icon = 'info'; break;
          case 'err': $cls = 'uk-alert-danger'; $icon = 'bell'; break;
          case 'succ': $cls = 'uk-alert-success'; $icon = 'check'; break;
          case 'warn': $cls = 'uk-alert-warning'; $icon = 'warning'; break;
        }
        $out .= "<div class='$cls' uk-alert><a class='uk-alert-close' uk-close></a><p><span class='uk-margin-right' uk-icon='icon: $icon'></span> {$message[1]}</p></div>";
      }

      // wrap everything in a card?
      if($out AND $card) $out = '<div class="uk-card uk-card-default uk-card-body uk-margin">'.$out.'</div>';

      return $out;
    }

    /**
     * show installation summary
     */
    public function renderSummary() {
      if(!$this->summary) return;

      $out = '<div class="uk-card uk-card-default uk-card-body uk-margin">
        <table class="uk-table uk-table-striped uk-table-small">
          <tbody>
            <tr>
              <td class="uk-width-small">Frontend URL</td>
              <td><a href="' . $this->wire->pages->get(1)->httpUrl . '">' . $this->wire->pages->get(1)->httpUrl . '</a></td>
            </tr>
            <tr>
              <td>Admin URL</td>
              <td><a href="' . $this->wire->pages->get(2)->httpUrl . '">' . $this->wire->pages->get(2)->httpUrl . '</a></td>
            </tr>
            <tr>
              <td>Admin Username</td>
              <td>' . $this->wire->users->get(41)->name . '</td>
            </tr>
            <tr>
              <td>Password</td>
              <td>' . $this->pwconfig['userpass'] . '</td>
            </tr>
            <tr>
              <td>E-Mail</td>
              <td>' . $this->pwconfig['useremail'] . '</td>
            </tr>
          </tbody>
        </table>
        <p>See the log below...</p>'.
        $this->branding().
        '</div>';

      return $out;
    }

    /**
     * return the branding markup
     */
    public function branding($slim = false) {
      $top = 0;
      $drink = '';
      $logo = '';
      if(!$slim) {
        $top = 35;
        $drink = '<br>If this tool helped you save some time please like and rate <a href="http://facebook.com/baumrock" target="_blank">my facebook page</a> or <a href="http://paypal.me/baumrock/5" target="_blank">buy me a drink</a>. Thanks! :)';
        $logo = '<br><a href="https://www.baumrock.com" target="_blank"><img src="https://gitlab.com/baumrock/kickstart/raw/master/assets/baumrock.png" style="height:40px;margin-top:10px;border:0;"></a>';
      }

      $out = '<div style="text-align:center;margin-top:'.$top.'px;">ProcessWire Kickstart powered by <a href="https://www.baumrock.com" target="_blank">baumrock.com</a>'.$drink.$logo.'</div>';
      return $out;
    }

  /****************************** recipe helper functions *******************************/
    /**
     * install module
     * either by the module name (for modules that ship with the core but are not installed)
     * or a module url pointing to a zip file
     */
    private function installModule($modulename, $url = null) {
      if($url) {
        $module = $this->wire->modules->get($modulename);
        if($module) {
          $this->warn("Module $modulename already exists");
          return $module;
        }
        else {
          $info = pathinfo($url);
          $ext = strtolower($info['extension']);
          if($ext != 'zip') {
            $this->err('Module url must be a zip file!');
            return;
          }
          
          $path = $this->siteRoot . "site/modules/";
          $zip = $path.$modulename.".zip";
          $this->downloadAndSave($url, $zip);
          $files = $this->extractZip($zip, $path);
          rename($path.$files[0], $path.$modulename);
        }
      }

      if(!@$this->wire->modules) {
        $this->err('wire->modules is not set');
        return;
      }

      $this->wire->modules->resetCache();
      $module = $this->wire->modules->get($modulename);
      $this->succ("Installed module $modulename");
      return $module;
    }
    
    /**
     * return random password
     */
    public function randomPassword($len = 8) {
      $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!$%&/()=?';
      $pass = array(); //remember to declare $pass as an array
      $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
      for ($i = 0; $i < $len; $i++) {
          $n = rand(0, $alphaLength);
          $pass[] = $alphabet[$n];
      }
      return implode($pass); //turn the array into a string
    }

  /****************************** processwire installer related functions *******************************/
  
    /**
     * set the active site profile
     */
    private function setSiteprofile() {
      $profile = @$this->config['profile'] ?: 'site-blank';

      // if the profile exists as folder just rename this folder
      if(is_dir($dir = $this->siteRoot.$profile)) {
        rename($dir, 'site');
        return;
      }

      // if the site profile is a local zipfile extract it to /site
      if(is_file($file = $this->siteRoot.$profile)) {
        $files = $this->extractZip($file);
        rename($files[0], 'site');
        return;
      }

      // otherwise it's an url
      $tmp = 'tmp-profile';
      $this->downloadAndSave($profile, "$tmp.zip");
      $files = $this->extractZip("$tmp.zip");
      rename($files[0], 'site');
    }

    /**
     * send data to pw-installer via curl post
     */
    public function postToPW($data, $options = []) {
      if(!function_exists('curl_version')) {
        $this->err('CURL is required for PW Kickstart');
        return;
      }

      // add pw config to data array
      $data = array_merge($data, $this->pwconfig);

      // do the curl request
      $url = $this->host.'/install.php';
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      $out = curl_exec($ch);
      curl_close ($ch);

      // if we got some html output we log it to the siteRoot
      $filename = 'log_'.$this->id.'_step'.(++$this->postStep);
      // one can add a name to the step to debug if the output fits to the request
      $step = isset($options['stepname']) ? $options['stepname'] : '';
      if($step) $filename .= "_$step";
      $filename .= '.htm';
      file_put_contents($this->siteRoot.$filename, $out);

      // check if this request was successful
      $step = $step ?: 'Step '.$this->postStep;
      $iframe = "<iframe src='/$filename'></iframe>";
      $errors = [
        'uk-alert-danger',
        'uk-text-danger',
        'o installation profile', // math "No" and "no"
        'This installer has already run',
      ];
      if(isset($options['errors']) AND is_array($options['errors'])) $errors = $options['errors'];
      foreach($errors as $error) {

        if(strpos($out, $error) !== false) {
          $this->err("Found errors at PW Installer \"$step\"<br>$iframe");
          return false;
        }
      }
      
      if(!@$options['quiet']) $this->succ("Successful request to PW Installer \"$step\"<br>$iframe");

      return $out;
    }

    /**
     * execute all recipes
     */
    private function executeRecipes() {
      // get the new processwire instance
      include($this->siteRoot . 'index.php');
      $this->wire = $wire;
      
      // force superuser for executing recipes
      $session->forceLogin($users->get(41));

      // load the backend until all updates are done
      $doupdate = true;      
	    while($doupdate) {
        $url = $wire->pages->get(2)->httpUrl;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $out = curl_exec($ch);
        curl_close ($ch);

        if(strpos($out, 'Update #') === false) $doupdate = false;
        else $this->succ('Installed internal update');
      }
      
      // now execute all recipes
      foreach($this->getRecipes() as $recipe) {
        if(is_callable($recipe)) call_user_func($recipe);
        else $this->runPHP($recipe);
      }

      // increase counter for pw-installations
      $http = new \ProcessWire\WireHttp();
      $http->post('http://www.google-analytics.com/collect', [ // Make sure to use HTTP, not HTTPS!
        'v' => 1, // Version
        'tid' => 'UA-76905506-1', // Tracking ID / Property ID.
        'cid' => 555, // Anonymous Client ID.
        't' => 'event', // hit type
        'ec' => 'PWModules', // category
        'ea' => 'finished', // action
        'el' => 'KickstartInstallation', // label
      ]);

      return true;
    }

    /**
     * return an array of all recipes
     */
    public function getRecipes() {
      $recipes = [];

      // download all recipes
      if(isset($this->config['recipes']) AND is_array($this->config['recipes'])) {
        foreach($this->config['recipes'] as $recipe) {
          
          // if recipe is a closure add it and continue
          if(is_callable($recipe)) {
            $recipes[] = $recipe;
            continue;
          }

          $info = pathinfo($recipe);
          $filename = $info['basename'];
          $ext = strtolower($info['extension']);
          
          // check file ending
          if($ext != 'php') {
            $this->err("Recipe must be a php file: $recipe");
            continue;
          }

          // download recipe
          if(!is_dir('recipes')) mkdir('recipes');
          $this->downloadAndSave($recipe, "recipes/$filename");

          // add it to array
          $recipes[] = $this->siteRoot . "recipes/$filename";
        }
      }

      return $recipes;
    }

  /****************************** file functions *******************************/
    
    /**
     * extract zipfile to given path
     */
    private function extractZip($zipfile, $path = null, $delete = true) {
      if(!$path) $path = $this->siteRoot;
      $zip = new \ZipArchive;
      if ($zip->open($zipfile) === TRUE) {
        for($i = 0; $i < $zip->numFiles; $i++) {
          $zip->extractTo($path, array($zip->getNameIndex($i)));
        }

        // return all files that are in the root folder of the ziparchive
        $i = 0;
        $files = [];
        while($file = $zip->getNameIndex($i)) {
          $i++;
          if(strpos(rtrim($file,'/'),'/')) continue;
          $files[] = $file;
        }
        $zip->close();
        
        // remove zip archive itself
        if($delete) unlink($zipfile);
      }
      else {
        $this->err('Error opening downloaded zipfile ' . $zipfile);
      }
      return $files;
    }

    /**
     * Recursively move files from one directory to another
     *
     * @param String $src - Source of files being moved
     * @param String $dest - Destination of files being moved
     */
    private function recursiveMove($src, $dest){
    
      // If source is not a directory stop processing
      if(!is_dir($src)) return false;
    
      // If the destination directory does not exist create it
      if(!is_dir($dest)) {
        if(!mkdir($dest)) {
          // If the destination directory could not be created stop processing
          return false;
        }
      }
    
      // Open the source directory to read in files
      $i = new \DirectoryIterator($src);
      foreach($i as $f) {
        if($f->isFile()) {
          rename($f->getRealPath(), "$dest/" . $f->getFilename());
        } else if(!$f->isDot() && $f->isDir()) {
          $this->recursiveMove($f->getRealPath(), "$dest/$f");
        }
      }

      // remove the temp folder where zip was extracted
      $this->removeDir($src, true);

      return true;
    }
    
    /**
     * recursively remove directory and it's content, this works great even where unlink() fails
     * @param  string $dir      folder to remove files
     * @param  boolean $deleteDir should folder be remove also set this to true
     */
    private function removeDir($dir, $deleteDir) {
      if(!$dh = @opendir($dir)) return;
      while (($obj = readdir($dh))) {
        if($obj=='.' || $obj=='..') continue;
        if (!@unlink($dir.'/'.$obj)) $this->removeDir($dir.'/'.$obj, true);
      }
      if ($deleteDir){
        closedir($dh);
        @rmdir($dir);
      }
    }

    /**
     * download given file to current working directory
     */
    public function downloadAndSave($url, $filename = null) {
      $info = pathinfo($url);
      $filename = $filename ?: $info['basename'];
      $ext = strtolower($info['extension']);

      // early exit on wrong url
      if($ext !== 'php' AND $ext !== 'zip') {
        $this->err("File must be PHP or ZIP ($url)");
        return;
      }

      // download file
        if((substr($url,0,8) == 'https://') && ! extension_loaded('openssl')) {
          $this->err("OpenSSL extension required but not available. File could not be downloaded from $url");
          return;
        }
        
        // Define the options
        $options = array('max_redirects' => 4);
        $context = stream_context_create(array('http' => $options));
      
        // download the zip
        if(!$content = @file_get_contents($url, $filename, $context)) {
          $this->err("File could not be downloaded: $url");
          return;
        }
      
        if(($fp = fopen($filename, 'wb')) === false) {
          $this->err("fopen error for filename $filename");
          return;
        }
      
        fwrite($fp, $content);
        fclose($fp);

      return true;
    }

  /****************************** others *******************************/
    
    /**
     * add msg/success/error messages to msg array
     */
    private function msg($str) { $this->messages[] = ['msg', $str]; }
    private function err($str) { $this->messages[] = ['err', $str]; $this->numerr++; }
    private function warn($str) { $this->messages[] = ['warn', $str]; }
    private function succ($str) { $this->messages[] = ['succ', $str]; }

    /**
     * execute the given php file
     */
    public function runPHP($filepath) {
      if(!$this->checkPHP($filepath)) return false;

      // define common api variables
      $wire = $this->wire;
      $config = $wire->config;
      $pages = $wire->pages;
      $recipesettings = isset($this->config['recipesettings']) ? $this->config['recipesettings'] : [];

      // include the recipe, load output in the buffer
      ob_start();
      include($filepath);
      $out = ob_get_clean();
      
      // save notice that we had some output
      if($out) $this->msg(nl2br("There was some output in your recipe (better use \$this->msg(), \$this->err, \$this->warn, \$this->succ):\n" . $out));

      return true;
    }

    /**
     * check the given php file for syntax errors
     */
    public function checkPHP($filepath) {
      exec("php -l $filepath", $out);
      if(@strpos($out[0], 'No syntax errors detected in') === 0) return true;
      else {
        $this->err(implode("<br>", array_filter($out)));
        return false;
      }
      return true;
    }

    /**
     * get config from kickstartfile
     */
    private function getConfig() {
      if(!is_file('kickstartfile.php')) {
        $this->err('No config found - please upload a kickstartfile!');
        return;
      }

      // eval this code
      $config = $this->eval($this->siteRoot . 'kickstartfile.php');
      if(!is_array($config)) {
        $this->err('Unable to load kickstart config');
        return;
      }

      return $config;
    }

    /**
     * eval code of file and return result
     */
    private function eval($filepath) {
      // check if file exists
      if(!is_file($filepath)) {
        $this->err("File $filepath not found");
        return;
      }
      
      // check syntax and show error
      if(!$this->checkPHP($this->siteRoot . 'kickstartfile.php')) return;

      // get file contents
      $code = file($filepath);

      // check if first line starts with php
      if(strpos($code[0], '<?php') !== 0) {
        $this->err("File $filepath has to begin with &gt;?php");
        return;
      }

      // remove first line from file
      unset($code[0]);

      return eval(implode("\n", $code));
    }

    /**
     * load tracy if present (for development)
     */
    private function tracy() {
      $tracy = $this->siteRoot . 'tracy/tracy.php';

      // dont try to load tracy if it does not exist (eg in the processmodule)
      if(!is_file($tracy)) {
        function bd($data) {}
        return;
      }

      // otherwise setup a basic tracy debug bar with the sort bd() method
      require_once($tracy);
      \Tracy\Debugger::enable();
      function bd($data) { \Tracy\Debugger::barDump($data); }
    }

    /**
     * cleanup all files that belong to kickstart and are not needed for PW
     */
    private function cleanUp() {
      // make sure we are in the site root
      chdir($this->siteRoot);

      // remove backup folder
      $this->removeDir('bak', 1);

      // remove recipe folder
      $this->removeDir('recipes', 1);

      // single files
      foreach (new \DirectoryIterator($this->siteRoot) as $fileinfo) {
        if(!$fileinfo->isFile()) continue;

        $file = $fileinfo->getBasename();
        if(strpos($file, 'log_'.$this->id) === 0) unlink($file); // delete all logfiles
        if($file == 'kickstartfile.php') unlink($file); // remove (and unset?) kickstartfile
      }

      // remove all iframes from logs because this would lead to 404s
      foreach($this->messages as $i=>$message) $this->messages[$i][1] = explode('<iframe', $message[1])[0];
      
      // remove the kickstart.php file itself
      unlink(__FILE__);
      $this->succ('Cleanup done');
    }

    /**
     * return how long the installation took in seconds
     */
    public function getDuration() {
      return round(microtime(true) - $this->start,2);
    }
}

// handle special requests
if(isset($_GET['action'])) {
  $action = $_GET['action'];
  if($action == 'upload') {
    $file = getcwd().'/kickstartfile.php';
    if(is_file($file)) {
      unlink($file);
    }
    $upload = move_uploaded_file($_FILES["files"]["tmp_name"][0], 'kickstartfile.php');
    echo json_encode(['upload'=>$upload]);
    die();
  }
}

// early exit when we bootstrap the installer
if(defined('bootstrap')) return;
$kick = new Kickstart();
?><!DOCTYPE html>
<html>
  <head>
    <title>PW Kickstart powered by baumrock.com</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="https://www.baumrock.com/site/templates/favicon/favicon.ico">

    <!-- CSS -->
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.31/css/uikit.min.css" />

    <!-- JS -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.31/js/uikit.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.31/js/uikit-icons.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/zepto/1.2.0/zepto.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/ace/1.2.9/ace.js"></script>

    <!-- custom styles -->
    <style>
    a:hover { text-decoration: none; }
    iframe { width: 100%; height: 400px; margin-top: 15px; }
    </style>
  </head>
  <body class="uk-background-muted">
  
    <section class="uk-section-primary">
      <div class="uk-container uk-text-center uk-padding-small">
        <a href="./kickstart.php" style="text-decoration: none;"><span uk-icon="icon: bolt"></span> ProcessWire Kickstart v<?= $kick->version ?></a>
        by <a href="https://www.baumrock.com">baumrock.com</a>
        <div class="uk-text-center uk-text-small">Need help or want to say thank you? <a href="https://processwire.com/talk/topic/18166-processwire-kickstart/" target="blank">Please visit the support forum thread :)</a></div>
      </div>
    </section>
  
    <section class="uk-container uk-padding" id="main" uk-height-viewport="expand: true">
      <form action="./kickstart.php" method="post" id="form">
        <?php
        $summary = $kick->renderSummary();
        echo $summary;
        ?>
        <?= $kick->renderMessages($card = true) ?>

        <?php if(!$summary): ?>
          <div class="uk-grid-match uk-child-width-1-2@s" uk-grid>
            <div>
              <div class="uk-card uk-card-default uk-card-body">
                <h4>Upload Kickstartfile</h4>
              
                <div class="js-upload uk-placeholder uk-text-center uk-margin-remove-bottom">
                  <span uk-icon="icon: cloud-upload"></span>
                  <span class="uk-text-middle">Drop your Kickstartfile here</span>
                  <div uk-form-custom>
                    <input type="file" multiple>
                    <span class="uk-link">or select it by clicking here</span>
                  </div>
                </div>
                <progress id="js-progressbar" class="uk-progress" value="0" max="100" hidden></progress>
              </div>
            </div>
            <div>
              <div class="uk-card uk-card-default uk-card-body">
                <h4>Load a Kickstartfile from URL</h4>
                <div class="uk-inline uk-width-1-1">
                  <span class="uk-form-icon" uk-icon="icon: github"></span>
                  <input class="uk-input" name="kickurl">
                </div>
                <p>Example Snippet from Gitlab Repo
                  <span class="uk-margin-left"><a href="<?= $kick->samplekickstartfile ?>" class="copy"><span uk-icon="icon: copy" title="Copy Link to Input"></span></a></span>
                  <span class="uk-margin-left"><a href="<?= $kick->samplekickstartfile ?>" target="_blank"><span uk-icon="icon: search" title="Preview File in a new Browser Tab"></span></a></span>
                </p>
                <button class="uk-button uk-button-default" type="submit" name="loadurl" id="loadurlbutton" value="1">Load</button>
              </div>
            </div>
          </div>

          <div class="uk-card uk-card-default uk-card-body uk-margin">
            <a class="uk-button uk-button-default uk-button-small" href="#defaults" uk-toggle>Show settings</a>

            <div id="defaults" class="uk-flex-top" uk-modal>
              <div class="uk-modal-dialog uk-modal-body uk-margin-auto-vertical">
                <pre><?php highlight_string("<?php\n" . var_export($kick->defaults, true)); ?></pre>
              </div>
            </div>

            <textarea id="editor"><?= @file_get_contents('kickstartfile.php'); ?></textarea>
            <textarea name="kickstartfile" class="uk-hidden"></textarea>
            <button class="uk-button uk-button-primary" type="submit" name="install" id="installbutton" value="1">Install</button>
          </div>

          <div id="loading" class="uk-flex-top" uk-modal>
            <div class="uk-modal-dialog uk-modal-body uk-margin-auto-vertical uk-text-center">
              <?= $kick->branding() ?><br><div uk-spinner></div><br><br><strong>Processing Installation - this may take a minute...</strong>
            </div>
          </div>

          
          <script>
            // uikit upload
            var bar = document.getElementById('js-progressbar');
            UIkit.upload('.js-upload', {
              url: './kickstart.php?action=upload',
              multiple: false,
              complete: function () {
                // reload window and reset post data
                window.location = window.location.href;
              },
              loadStart: function (e) {
                bar.removeAttribute('hidden');
                bar.max = e.total;
                bar.value = e.loaded;
              },
              progress: function (e) { bar.max = e.total; bar.value = e.loaded; },
              loadEnd: function (e) { bar.max = e.total; bar.value = e.loaded; },
              allow: '*.(php)',
            });

            // copy link to input
            $('a.copy').on('click', function() {
              $('input[name=kickurl]').val($(this).attr('href')).change();
              return false;
            });

            // show loadurl button when url field is not empty
            $('#loadurlbutton').hide();
            $('input[name=kickurl]').on('change keyup keydown', function(e) {
              if($(e.target).val()) $('#loadurlbutton').show();
              else $('#loadurlbutton').hide();
            });

            // ace editor
            var editor = ace.edit("editor");
            editor.getSession().setMode("ace/mode/php");
            editor.setTheme("ace/theme/monokai");
            editor.setOptions({
              maxLines: Infinity,
              tabSize: 2,
            });
            editor.focus(); //To focus the ace editor
            var n = editor.getSession().getValue().split("\n").length; // To count total no. of lines
            editor.gotoLine(n); //Go to end of document
            editor.execCommand("gotolineend"); // go to end of line

            // update textarea value on form submit
            $('#form').on('submit', function() {
              $('textarea[name=kickstartfile]').val(editor.getSession().getValue());
            });

            // install on ctrl+enter
            $('pre.ace_editor').keydown(function (e) {
              if (e.ctrlKey && e.keyCode == 13) {
                $('#installbutton').click();
              }
            });

            // show modal on installation
            $('#installbutton').click(function() { UIkit.modal('#loading').show(); });
          </script>

        <?php endif; ?>

      </form>
    </section>

    <section class="uk-background-secondary">
      <div class="uk-container uk-text-center uk-padding-small">
        <?= $kick->branding('slim') ?>
      </div>
    </section>
    
  </body>
</html>